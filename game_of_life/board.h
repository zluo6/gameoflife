#ifndef BOARD_H
#define BOARD_H

#include "QtCore\QDebug"

#include <QtWidgets\QWidget>
#include <QtGui\QtGui>
#include "ui_board.h"
#include "grid.h"
#include <QtWidgets\QFrame>

const int WIDTH = 15;

class Board : public QWidget
{
	Q_OBJECT

public:
	Board(int myOption = 0, int myRow = 30, int myCol = 30, int myInterval = 2000, QWidget * parent = 0);
	~Board();

	int getGen() const;
	void changeOpt(int opt);
	void changeSpeed(int speed);
	void step();
	void millionStep();
	int getRow();
	int getCol();

	public slots: 
		void slot_step();
		void slot_run();
		void slot_pause();
		void slot_clear();

signals:
		void genChanged(int gen);

protected:
	void paintEvent(QPaintEvent * e);

private:
	Ui::BoardClass ui;
	// helper function
	void drawSquare(QPainter & painter, int x, int y, QColor myColor);
	// variables
	int row;
	int col;
	int option;
	int generation;
	int interval;
	Grid * myGrid;
	QTimer * timer;
};

#endif // BOARD_H
