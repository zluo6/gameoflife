#include "window.h"

Window::Window(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	myBoard = new Board;

	connect(myBoard, SIGNAL(genChanged(int)), ui.lcdGenerations, SLOT(display(int)));

	setEnabled(false, false, false, false, false);

	mThread = new MyThread(this, myBoard);
	connect(mThread, SIGNAL(done()), this, SLOT(millionStep()));

	QGridLayout * layout = new QGridLayout;
	QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	myBoard->setSizePolicy(sizePolicy);
	myBoard->setMinimumSize(QSize(myBoard->getRow()*WIDTH, myBoard->getCol()*WIDTH));
	layout->addWidget(ui.verticalLayoutWidget, 0, 0);
	layout->addWidget(myBoard, 0, 1, 1, 1);
	layout->addWidget(ui.verticalLayoutWidget_2, 1, 0);
	layout->addWidget(ui.gridLayoutWidget_2, 1, 1);
	setLayout(layout);
}

Window::~Window()
{

}

void Window::build()
{
	myBoard->changeOpt(option);
}

void Window::setEnabled(bool step, bool run, bool pause, bool clear, bool million)
{
	ui.Step->setEnabled(step);
	ui.Run->setEnabled(run); 
	ui.Pause->setEnabled(pause);
	ui.Clear->setEnabled(clear);
	ui.Million->setEnabled(million);
}

void Window::on_Blinker_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 1; ui.label->setText("Blinker"); build();}
void Window::on_Toad_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 2; ui.label->setText("Toad"); build();}
void Window::on_Beacon_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 3; ui.label->setText("Beacon"); build();}
void Window::on_Pulsar_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 4; ui.label->setText("Pulsar"); build();}
void Window::on_Glider_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 5; ui.label->setText("Glider"); build();}
void Window::on_Light_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 6; ui.label->setText("Lightweight spaceship"); build();}
void Window::on_Queen_clicked(){setEnabled(true, true, false, false, true); myBoard->slot_clear(); option = 7; ui.label->setText("The Queen Bee Shuttle"); build();}

void Window::on_Step_clicked(){setEnabled(true, true, false, true, true); myBoard->slot_step();}
void Window::on_Run_clicked(){setEnabled(false, false, true, false, false); myBoard->slot_run();}
void Window::on_Pause_clicked(){setEnabled(true, true, false, true, true); myBoard->slot_pause();}
void Window::on_Clear_clicked(){setEnabled(false, false, false, false, false); myBoard->slot_clear();}

void Window::on_horizontalSlider_valueChanged()
{
	int interval = ui.horizontalSlider->value()*(-19)+2000;
	ui.lcdSpeed->display(ui.horizontalSlider->value());
	myBoard->changeSpeed(interval);
}

void Window::on_Million_clicked()
{
	this->setDisabled(true);
	mThread->start();
}

void Window::millionStep()
{
	this->setDisabled(false);
	setEnabled(false, false, false, true, false);
	ui.lcdGenerations->display(-1);
}



