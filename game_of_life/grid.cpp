#include "grid.h"

// constructor1
Grid::Grid()
{
	build(0, 20, 20);
}

// constructor2
Grid::Grid(int myOption, int myRow, int myCol)
{
	if ((myRow >= 20)&&(myCol >= 20))
	{
		build(myOption, myRow, myCol);
		seed(option);
	}
	else
	{
		build(0, 20, 20);
	}
}

// copy constructor
Grid::Grid(Grid const & other)
{
	copy(other);
}

// destructor
Grid::~Grid()
{
	clear();
}

// assignment operator
Grid const & Grid::operator=(Grid const & other)
{
	if (this != &other)
	{
		clear();
		copy(other);
	}
	return *this;
}

void Grid::build(int myOption, int myRow, int myCol)
{
	row = myRow;
	col = myCol;
	option = myOption;

	grid = new int * [row];
	buffer = new int * [row];

	for (int i = 0; i < row; i++)
	{
		grid[i] = new int[col];
		buffer[i] = new int[col];
	}

	clearGrid();
}

void Grid::clear()
{
	for(int i = 0; i < row; i++)
	{
		delete [] grid[i];
		delete [] buffer[i];
	}
	delete [] grid;
	delete [] buffer;
}

void Grid::copy(Grid const & other)
{
	build(other.option, other.row, other.col);

	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			grid[i][j] = other.grid[i][j];
			buffer[i][j] = other.buffer[i][j];
		}
	}
}

void Grid::clearGrid()
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			grid[i][j] = 0;
			buffer[i][j] = 0;
		}
	}
}

void Grid::checkCell(int curRow, int curCol)
{
	int neighbors = 0;

	// count neighbors
	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			// check if it is not out of range
			if ((curRow + i >= 0)&&(curRow + i < row)&&(curCol + j >= 0)&&(curCol + j < col))
			{
				if ((!((i == 0)&&(j == 0)))&&(grid[curRow + i][curCol + j]))
				{
					neighbors++;
				}
			}
		}
	}

	// apply rules
	if (grid[curRow][curCol])
	{
		// Any live cell with less than 2 live neighbours dies, as if caused by underpopulation.
		// Any live cell with more than 3 live neighbours dies, as if by overcrowding.
		if ((neighbors < 2)||(neighbors > 3))
		{
			buffer[curRow][curCol] = 0;
		}
		// Any live cell with 2 or 3 live neighbours lives on to the next generation.
		else
		{
			buffer[curRow][curCol] = 1;
		}
	}
	else
	{
		// Any dead cell with exactly 3 live neighbours becomes a live cell.
		if (neighbors == 3)
		{
			buffer[curRow][curCol] = 1;
		}
	}
}

void Grid::step()
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			checkCell(i, j);
		}
	}
	for (int k = 0; k < row; k++)
	{
		for (int l = 0; l < col; l++)
		{
			grid[k][l] = buffer[k][l];
		}
	}
}

int Grid::getGrid(int curRow, int curCol)
{
	if ((curRow < row)&&(curCol < col)) return grid[curRow][curCol];
	else return -1;
}

void Grid::seed(int opt)
{
	option = opt;
	clearGrid();
	int midRow = row / 2;
	int midCol = col / 2;

	switch (option)
	{
	case 1: // Blinker
		{
			grid[midRow-1][midCol] = 1;
			grid[midRow][midCol] = 1;
			grid[midRow+1][midCol] = 1;
			break;
		}
	case 2: // Toad
		{
			grid[midRow-2][midCol-1] = 1;
			grid[midRow-2][midCol] = 1;
			grid[midRow-2][midCol+1] = 1;
			grid[midRow-1][midCol-2] = 1;
			grid[midRow-1][midCol-1] = 1;
			grid[midRow-1][midCol] = 1;
			break;
		}
	case 3: // Beacon
		{
			grid[midRow-2][midCol-3] = 1;
			grid[midRow-2][midCol-2] = 1;
			grid[midRow-1][midCol-3] = 1;
			grid[midRow-1][midCol-2] = 1;
			grid[midRow][midCol-1] = 1;
			grid[midRow][midCol] = 1;
			grid[midRow+1][midCol-1] = 1;
			grid[midRow+1][midCol] = 1;
			break;
		}
	case 4: // Pulsar
		{
			grid[midRow-6][midCol-4] = 1;
			grid[midRow-6][midCol-3] = 1;
			grid[midRow-6][midCol-2] = 1;
			grid[midRow-6][midCol+2] = 1;
			grid[midRow-6][midCol+3] = 1;
			grid[midRow-6][midCol+4] = 1;
			grid[midRow-4][midCol-6] = 1;
			grid[midRow-4][midCol-1] = 1;
			grid[midRow-4][midCol+1] = 1;
			grid[midRow-4][midCol+6] = 1;
			grid[midRow-3][midCol-6] = 1;
			grid[midRow-3][midCol-1] = 1;
			grid[midRow-3][midCol+1] = 1;
			grid[midRow-3][midCol+6] = 1;
			grid[midRow-2][midCol-6] = 1;
			grid[midRow-2][midCol-1] = 1;
			grid[midRow-2][midCol+1] = 1;
			grid[midRow-2][midCol+6] = 1;
			grid[midRow-1][midCol-4] = 1;
			grid[midRow-1][midCol-3] = 1;
			grid[midRow-1][midCol-2] = 1;
			grid[midRow-1][midCol+2] = 1;
			grid[midRow-1][midCol+3] = 1;
			grid[midRow-1][midCol+4] = 1;

			grid[midRow+6][midCol-4] = 1;
			grid[midRow+6][midCol-3] = 1;
			grid[midRow+6][midCol-2] = 1;
			grid[midRow+6][midCol+2] = 1;
			grid[midRow+6][midCol+3] = 1;
			grid[midRow+6][midCol+4] = 1;
			grid[midRow+4][midCol-6] = 1;
			grid[midRow+4][midCol-1] = 1;
			grid[midRow+4][midCol+1] = 1;
			grid[midRow+4][midCol+6] = 1;
			grid[midRow+3][midCol-6] = 1;
			grid[midRow+3][midCol-1] = 1;
			grid[midRow+3][midCol+1] = 1;
			grid[midRow+3][midCol+6] = 1;
			grid[midRow+2][midCol-6] = 1;
			grid[midRow+2][midCol-1] = 1;
			grid[midRow+2][midCol+1] = 1;
			grid[midRow+2][midCol+6] = 1;
			grid[midRow+1][midCol-4] = 1;
			grid[midRow+1][midCol-3] = 1;
			grid[midRow+1][midCol-2] = 1;
			grid[midRow+1][midCol+2] = 1;
			grid[midRow+1][midCol+3] = 1;
			grid[midRow+1][midCol+4] = 1;
			break;
		}
	case 5: // Glider
		{
			grid[0][0] = 1;
			grid[0][2] = 1;
			grid[1][1] = 1;
			grid[1][2] = 1;
			grid[2][1] = 1;
			break;
		}
	case 6: // Lightweight spaceship
		{
			grid[2][3] = 1;
			grid[2][4] = 1;
			grid[2][5] = 1;
			grid[2][6] = 1;
			grid[3][2] = 1;
			grid[3][6] = 1;
			grid[4][6] = 1;
			grid[5][2] = 1;
			grid[5][5] = 1;
			break;
		}
	case 7: // The Queen Bee Shuttle
		{
			grid[midRow-3][midCol-3] = 1;
			grid[midRow-3][midCol-2] = 1;
			grid[midRow-2][midCol-1] = 1;
			grid[midRow-1][midCol] = 1;
			grid[midRow][midCol] = 1;
			grid[midRow+1][midCol] = 1;
			grid[midRow+2][midCol-1] = 1;
			grid[midRow+3][midCol-2] = 1;
			grid[midRow+3][midCol-3] = 1;
			break;
		}
	}
}