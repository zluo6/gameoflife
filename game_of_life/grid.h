#ifndef GRID_H
#define GRID_H

/* Rules:
* - Any live cell with less than 2 live neighbours dies, as if caused by underpopulation.
* - Any live cell with more than 3 live neighbours dies, as if by overcrowding.
* - Any live cell with 2 or 3 live neighbours lives on to the next generation.
* - Any dead cell with exactly 3 live neighbours becomes a live cell.
*
* My rule: row, col should be greater than 19.
*/

class Grid
{
public:
	// constructor
	Grid();
	Grid(int myOption, int myRow, int myCol);
	// big three
	// copy constructor
	Grid(Grid const & other);
	// destructor
	~Grid();
	// assignment operator
	Grid const & operator=(Grid const & other);

	void step();
	int getGrid(int curRow, int curCol);
	void seed(int opt);
	void clearGrid();

private:
	// helper function
	void build(int myOption, int myRow, int myCol);
	void clear();
	void copy(Grid const & other);
	void checkCell(int curRow, int curCol);

	// variables
	int row;
	int col;
	int option;
	int ** grid;
	int ** buffer;
};
#endif // GRID_H