#include "board.h"

Board::Board(int myOption, int myRow, int myCol, int myInterval, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	option = myOption;
	row = myRow;
	col = myCol;
	interval = myInterval;
	generation = 0;
	myGrid = new Grid(option, row, col);
	timer = new QTimer(this);
	timer->setInterval(interval);
	connect(timer, SIGNAL(timeout()), this, SLOT(slot_step()));
	// resize(WIDTH * row, WIDTH * col);
}

Board::~Board()
{
	delete myGrid;
}

void Board::paintEvent(QPaintEvent * e)
{
	QPainter painter(this);
	QRect rect = contentsRect();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (myGrid->getGrid(i, j) == 1) drawSquare(painter, rect.left() + j * WIDTH, rect.top() + i * WIDTH, Qt::yellow);
			else if (myGrid->getGrid(i, j) == -1) drawSquare(painter, rect.left() + j * WIDTH, rect.top() + i * WIDTH, Qt::black); // error
			else if (myGrid->getGrid(i, j) == 0) drawSquare(painter, rect.left() + j * WIDTH, rect.top() + i * WIDTH, Qt::lightGray);
		}
	}
}

void Board::drawSquare(QPainter & painter, int x, int y, QColor myColor)
{
	painter.fillRect(x + 2, y + 2, WIDTH - 3, WIDTH - 3, myColor);

	painter.setPen(Qt::black);
	painter.drawLine(x, y + WIDTH - 2, x, y);
	painter.drawLine(x, y, x + WIDTH - 2, y);

	painter.setPen(Qt::darkGray);
	painter.drawLine(x + 2, y + WIDTH - 2, x + WIDTH - 2, y + WIDTH - 2);
	painter.drawLine(x + WIDTH - 2, y + WIDTH - 2, x + WIDTH - 2, y + 2);
}

int Board::getGen() const
{
	return generation;
}

void Board::changeOpt(int opt)
{
	option = opt;
	generation = 0;
	myGrid->seed(option);
}

void Board::changeSpeed(int newInterval)
{
	interval = newInterval;
	timer->setInterval(interval);
}

void Board::step()
{
	generation++;
	emit genChanged(generation);
	myGrid->step();
	update();
}

void Board::millionStep()
{
	myGrid->step();
}

void Board::slot_run()
{
	timer->start();
}

void Board::slot_pause()
{
	timer->stop();
}

void Board::slot_step()
{
	step();
}

void Board::slot_clear()
{
	timer->stop();
	myGrid->clearGrid();
	generation = 0;
	emit genChanged(generation);
	update();
}

int Board::getRow() {return row;}
int Board::getCol() {return col;}

