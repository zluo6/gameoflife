/********************************************************************************
** Form generated from reading UI file 'window.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_H
#define UI_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Window
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *Osciallators;
    QPushButton *Blinker;
    QPushButton *Toad;
    QPushButton *Beacon;
    QPushButton *Pulsar;
    QSpacerItem *verticalSpacer;
    QLabel *Spaceships;
    QPushButton *Glider;
    QPushButton *Light;
    QPushButton *Queen;
    QSpacerItem *verticalSpacer_2;
    QLabel *Control;
    QPushButton *Step;
    QPushButton *Run;
    QPushButton *Pause;
    QPushButton *Clear;
    QWidget *gridLayoutWidget_2;
    QGridLayout *displayLayout;
    QSlider *horizontalSlider;
    QLCDNumber *lcdSpeed;
    QLCDNumber *lcdGenerations;
    QLabel *label;
    QSpacerItem *verticalSpacer_3;
    QWidget *verticalLayoutWidget_2;
    QGridLayout *gridLayout;
    QLabel *Generations;
    QLabel *Speed;
    QPushButton *Million;

    void setupUi(QWidget *Window)
    {
        if (Window->objectName().isEmpty())
            Window->setObjectName(QStringLiteral("Window"));
        Window->resize(630, 630);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Window->sizePolicy().hasHeightForWidth());
        Window->setSizePolicy(sizePolicy);
        Window->setMinimumSize(QSize(630, 600));
        Window->setMaximumSize(QSize(660, 630));
        Window->setMouseTracking(false);
        Window->setAcceptDrops(false);
        Window->setWindowOpacity(0.9);
        Window->setAutoFillBackground(false);
        verticalLayoutWidget = new QWidget(Window);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 139, 453));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(10, 10, 10, 10);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        Osciallators = new QLabel(verticalLayoutWidget);
        Osciallators->setObjectName(QStringLiteral("Osciallators"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(Osciallators->sizePolicy().hasHeightForWidth());
        Osciallators->setSizePolicy(sizePolicy1);
        Osciallators->setMinimumSize(QSize(119, 30));
        Osciallators->setMaximumSize(QSize(180, 50));
        QFont font;
        font.setPointSize(12);
        Osciallators->setFont(font);
        Osciallators->setAutoFillBackground(true);
        Osciallators->setFrameShape(QFrame::Box);
        Osciallators->setFrameShadow(QFrame::Sunken);
        Osciallators->setLineWidth(2);
        Osciallators->setScaledContents(false);
        Osciallators->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(Osciallators);

        Blinker = new QPushButton(verticalLayoutWidget);
        Blinker->setObjectName(QStringLiteral("Blinker"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Blinker->sizePolicy().hasHeightForWidth());
        Blinker->setSizePolicy(sizePolicy2);
        Blinker->setMinimumSize(QSize(119, 23));
        Blinker->setMaximumSize(QSize(180, 50));
        Blinker->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Blinker);

        Toad = new QPushButton(verticalLayoutWidget);
        Toad->setObjectName(QStringLiteral("Toad"));
        sizePolicy2.setHeightForWidth(Toad->sizePolicy().hasHeightForWidth());
        Toad->setSizePolicy(sizePolicy2);
        Toad->setMinimumSize(QSize(119, 23));
        Toad->setMaximumSize(QSize(180, 50));
        Toad->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Toad);

        Beacon = new QPushButton(verticalLayoutWidget);
        Beacon->setObjectName(QStringLiteral("Beacon"));
        sizePolicy2.setHeightForWidth(Beacon->sizePolicy().hasHeightForWidth());
        Beacon->setSizePolicy(sizePolicy2);
        Beacon->setMinimumSize(QSize(119, 23));
        Beacon->setMaximumSize(QSize(180, 50));
        Beacon->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Beacon);

        Pulsar = new QPushButton(verticalLayoutWidget);
        Pulsar->setObjectName(QStringLiteral("Pulsar"));
        sizePolicy2.setHeightForWidth(Pulsar->sizePolicy().hasHeightForWidth());
        Pulsar->setSizePolicy(sizePolicy2);
        Pulsar->setMinimumSize(QSize(119, 23));
        Pulsar->setMaximumSize(QSize(180, 50));
        Pulsar->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Pulsar);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        Spaceships = new QLabel(verticalLayoutWidget);
        Spaceships->setObjectName(QStringLiteral("Spaceships"));
        sizePolicy1.setHeightForWidth(Spaceships->sizePolicy().hasHeightForWidth());
        Spaceships->setSizePolicy(sizePolicy1);
        Spaceships->setMinimumSize(QSize(119, 30));
        Spaceships->setMaximumSize(QSize(180, 50));
        Spaceships->setFont(font);
        Spaceships->setAutoFillBackground(true);
        Spaceships->setFrameShape(QFrame::Box);
        Spaceships->setFrameShadow(QFrame::Sunken);
        Spaceships->setLineWidth(2);
        Spaceships->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(Spaceships);

        Glider = new QPushButton(verticalLayoutWidget);
        Glider->setObjectName(QStringLiteral("Glider"));
        sizePolicy2.setHeightForWidth(Glider->sizePolicy().hasHeightForWidth());
        Glider->setSizePolicy(sizePolicy2);
        Glider->setMinimumSize(QSize(119, 23));
        Glider->setMaximumSize(QSize(180, 50));
        Glider->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Glider);

        Light = new QPushButton(verticalLayoutWidget);
        Light->setObjectName(QStringLiteral("Light"));
        sizePolicy2.setHeightForWidth(Light->sizePolicy().hasHeightForWidth());
        Light->setSizePolicy(sizePolicy2);
        Light->setMinimumSize(QSize(119, 23));
        Light->setMaximumSize(QSize(180, 50));
        Light->setCursor(QCursor(Qt::PointingHandCursor));
        Light->setAutoDefault(false);
        Light->setDefault(false);
        Light->setFlat(false);

        verticalLayout_2->addWidget(Light);

        Queen = new QPushButton(verticalLayoutWidget);
        Queen->setObjectName(QStringLiteral("Queen"));
        sizePolicy2.setHeightForWidth(Queen->sizePolicy().hasHeightForWidth());
        Queen->setSizePolicy(sizePolicy2);
        Queen->setMinimumSize(QSize(119, 23));
        Queen->setMaximumSize(QSize(180, 50));
        Queen->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Queen);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        Control = new QLabel(verticalLayoutWidget);
        Control->setObjectName(QStringLiteral("Control"));
        sizePolicy1.setHeightForWidth(Control->sizePolicy().hasHeightForWidth());
        Control->setSizePolicy(sizePolicy1);
        Control->setMinimumSize(QSize(119, 30));
        Control->setMaximumSize(QSize(180, 50));
        Control->setFont(font);
        Control->setAutoFillBackground(true);
        Control->setFrameShape(QFrame::Box);
        Control->setFrameShadow(QFrame::Sunken);
        Control->setLineWidth(2);
        Control->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(Control);

        Step = new QPushButton(verticalLayoutWidget);
        Step->setObjectName(QStringLiteral("Step"));
        sizePolicy2.setHeightForWidth(Step->sizePolicy().hasHeightForWidth());
        Step->setSizePolicy(sizePolicy2);
        Step->setMinimumSize(QSize(119, 23));
        Step->setMaximumSize(QSize(180, 50));
        Step->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Step);

        Run = new QPushButton(verticalLayoutWidget);
        Run->setObjectName(QStringLiteral("Run"));
        sizePolicy2.setHeightForWidth(Run->sizePolicy().hasHeightForWidth());
        Run->setSizePolicy(sizePolicy2);
        Run->setMinimumSize(QSize(119, 23));
        Run->setMaximumSize(QSize(180, 50));
        Run->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Run);

        Pause = new QPushButton(verticalLayoutWidget);
        Pause->setObjectName(QStringLiteral("Pause"));
        sizePolicy2.setHeightForWidth(Pause->sizePolicy().hasHeightForWidth());
        Pause->setSizePolicy(sizePolicy2);
        Pause->setMinimumSize(QSize(119, 23));
        Pause->setMaximumSize(QSize(180, 50));
        Pause->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Pause);

        Clear = new QPushButton(verticalLayoutWidget);
        Clear->setObjectName(QStringLiteral("Clear"));
        sizePolicy2.setHeightForWidth(Clear->sizePolicy().hasHeightForWidth());
        Clear->setSizePolicy(sizePolicy2);
        Clear->setMinimumSize(QSize(119, 23));
        Clear->setMaximumSize(QSize(180, 50));
        Clear->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(Clear);

        gridLayoutWidget_2 = new QWidget(Window);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(160, 470, 451, 121));
        displayLayout = new QGridLayout(gridLayoutWidget_2);
        displayLayout->setSpacing(6);
        displayLayout->setContentsMargins(10, 10, 10, 10);
        displayLayout->setObjectName(QStringLiteral("displayLayout"));
        displayLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSlider = new QSlider(gridLayoutWidget_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(horizontalSlider->sizePolicy().hasHeightForWidth());
        horizontalSlider->setSizePolicy(sizePolicy3);
        horizontalSlider->setMinimumSize(QSize(0, 30));
        horizontalSlider->setMaximumSize(QSize(300, 30));
        horizontalSlider->setOrientation(Qt::Horizontal);

        displayLayout->addWidget(horizontalSlider, 1, 1, 1, 1);

        lcdSpeed = new QLCDNumber(gridLayoutWidget_2);
        lcdSpeed->setObjectName(QStringLiteral("lcdSpeed"));
        sizePolicy3.setHeightForWidth(lcdSpeed->sizePolicy().hasHeightForWidth());
        lcdSpeed->setSizePolicy(sizePolicy3);
        lcdSpeed->setMinimumSize(QSize(80, 30));
        lcdSpeed->setMaximumSize(QSize(175, 50));

        displayLayout->addWidget(lcdSpeed, 1, 0, 1, 1);

        lcdGenerations = new QLCDNumber(gridLayoutWidget_2);
        lcdGenerations->setObjectName(QStringLiteral("lcdGenerations"));
        sizePolicy3.setHeightForWidth(lcdGenerations->sizePolicy().hasHeightForWidth());
        lcdGenerations->setSizePolicy(sizePolicy3);
        lcdGenerations->setMinimumSize(QSize(80, 30));
        lcdGenerations->setMaximumSize(QSize(175, 50));

        displayLayout->addWidget(lcdGenerations, 0, 0, 1, 1);

        label = new QLabel(gridLayoutWidget_2);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);
        label->setMinimumSize(QSize(280, 30));
        label->setMaximumSize(QSize(280, 50));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        label->setFont(font1);
        label->setFrameShape(QFrame::StyledPanel);
        label->setFrameShadow(QFrame::Sunken);
        label->setLineWidth(2);

        displayLayout->addWidget(label, 0, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        displayLayout->addItem(verticalSpacer_3, 2, 0, 1, 2);

        verticalLayoutWidget_2 = new QWidget(Window);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 470, 139, 121));
        gridLayout = new QGridLayout(verticalLayoutWidget_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(10, 10, 10, 10);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        Generations = new QLabel(verticalLayoutWidget_2);
        Generations->setObjectName(QStringLiteral("Generations"));
        sizePolicy2.setHeightForWidth(Generations->sizePolicy().hasHeightForWidth());
        Generations->setSizePolicy(sizePolicy2);
        Generations->setMinimumSize(QSize(119, 30));
        Generations->setMaximumSize(QSize(180, 50));
        Generations->setFont(font);
        Generations->setAutoFillBackground(true);
        Generations->setFrameShape(QFrame::Box);
        Generations->setFrameShadow(QFrame::Sunken);
        Generations->setLineWidth(2);
        Generations->setTextFormat(Qt::AutoText);
        Generations->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(Generations, 0, 0, 1, 1);

        Speed = new QLabel(verticalLayoutWidget_2);
        Speed->setObjectName(QStringLiteral("Speed"));
        sizePolicy2.setHeightForWidth(Speed->sizePolicy().hasHeightForWidth());
        Speed->setSizePolicy(sizePolicy2);
        Speed->setMinimumSize(QSize(119, 30));
        Speed->setMaximumSize(QSize(180, 50));
        Speed->setFont(font);
        Speed->setLayoutDirection(Qt::LeftToRight);
        Speed->setAutoFillBackground(true);
        Speed->setFrameShape(QFrame::Box);
        Speed->setFrameShadow(QFrame::Sunken);
        Speed->setLineWidth(2);
        Speed->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(Speed, 1, 0, 1, 1);

        Million = new QPushButton(verticalLayoutWidget_2);
        Million->setObjectName(QStringLiteral("Million"));
        sizePolicy2.setHeightForWidth(Million->sizePolicy().hasHeightForWidth());
        Million->setSizePolicy(sizePolicy2);
        Million->setMinimumSize(QSize(119, 23));
        Million->setMaximumSize(QSize(180, 50));

        gridLayout->addWidget(Million, 2, 0, 1, 1);


        retranslateUi(Window);

        QMetaObject::connectSlotsByName(Window);
    } // setupUi

    void retranslateUi(QWidget *Window)
    {
        Window->setWindowTitle(QApplication::translate("Window", "Game of Life", 0));
        Osciallators->setText(QApplication::translate("Window", "Osciallators", 0));
        Blinker->setText(QApplication::translate("Window", "Blinker", 0));
        Toad->setText(QApplication::translate("Window", "Toad", 0));
        Beacon->setText(QApplication::translate("Window", "Beacon", 0));
        Pulsar->setText(QApplication::translate("Window", "Pulsar", 0));
        Spaceships->setText(QApplication::translate("Window", "Spaceships", 0));
        Glider->setText(QApplication::translate("Window", "Glider", 0));
        Light->setText(QApplication::translate("Window", "Lightweight spaceship", 0));
        Queen->setText(QApplication::translate("Window", "The Queen Bee Shuttle", 0));
        Control->setText(QApplication::translate("Window", "Control", 0));
        Step->setText(QApplication::translate("Window", "Step", 0));
        Run->setText(QApplication::translate("Window", "Run", 0));
        Pause->setText(QApplication::translate("Window", "Pause", 0));
        Clear->setText(QApplication::translate("Window", "Clear", 0));
        label->setText(QApplication::translate("Window", "Waiting...", 0));
        Generations->setText(QApplication::translate("Window", "Generations", 0));
        Speed->setText(QApplication::translate("Window", "Speed", 0));
        Million->setText(QApplication::translate("Window", "A Million Step", 0));
    } // retranslateUi

};

namespace Ui {
    class Window: public Ui_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_H
