/********************************************************************************
** Form generated from reading UI file 'board.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOARD_H
#define UI_BOARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BoardClass
{
public:

    void setupUi(QWidget *BoardClass)
    {
        if (BoardClass->objectName().isEmpty())
            BoardClass->setObjectName(QStringLiteral("BoardClass"));
        BoardClass->resize(600, 400);

        retranslateUi(BoardClass);

        QMetaObject::connectSlotsByName(BoardClass);
    } // setupUi

    void retranslateUi(QWidget *BoardClass)
    {
        BoardClass->setWindowTitle(QApplication::translate("BoardClass", "Board", 0));
    } // retranslateUi

};

namespace Ui {
    class BoardClass: public Ui_BoardClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOARD_H
