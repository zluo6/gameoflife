#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QtCore\QThread>

#include "board.h"

class MyThread : public QThread
{
	Q_OBJECT

public:
	MyThread(QObject *parent, Board * board);
	~MyThread();
	void run();

signals:
	void done();

private:
	Board * myBoard;
};

#endif // MYTHREAD_H
