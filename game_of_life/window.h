#ifndef WINDOW_H
#define WINDOW_H

#include <QtWidgets\QWidget>
#include <QtWidgets\QHBoxLayout>
#include "ui_window.h"
#include "board.h"
#include "mythread.h"

class Window : public QWidget
{
	Q_OBJECT

public:
	Window(QWidget *parent = 0);
	~Window();

private:
	void build();
	void setEnabled(bool step, bool run, bool pause, bool clear, bool million);

	Ui::Window ui;
	Board * myBoard;
	int side;
	int option;
	MyThread * mThread;

	public slots: 
		void on_Blinker_clicked();
		void on_Toad_clicked();
		void on_Beacon_clicked();
		void on_Pulsar_clicked();
		void on_Glider_clicked();
		void on_Light_clicked();
		void on_Queen_clicked();

		void on_Step_clicked();
		void on_Run_clicked();
		void on_Pause_clicked();
		void on_Clear_clicked();

		void on_horizontalSlider_valueChanged();

		void on_Million_clicked();
		void millionStep();
};
#endif // WINDOW_H
