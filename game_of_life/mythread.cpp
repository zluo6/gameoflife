#include "mythread.h"

MyThread::MyThread(QObject *parent, Board * board)
	: QThread(parent)
{
	myBoard = board;
}

MyThread::~MyThread()
{

}

void MyThread::run()
{
	for (int i = 0; i < 100000; i++)
	{
		myBoard->millionStep();
	}
	emit done();
}

